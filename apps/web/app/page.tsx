import { headers } from "next/headers";
import {Main} from '../widgets/main'

async function getData(path) {
  const res = await fetch(`${path}/api/tpl`, { cache: 'no-store' })
  // The return value is *not* serialized
  // You can return Date, Map, Set, etc.
 
  // Recommendation: handle errors
  if (!res.ok) {
    // This will activate the closest `error.js` Error Boundary
    throw new Error('Failed to fetch data')
  }
 
  return res.json()
}

export default async function Page() {

  const host = headers().get("host");
  const proto = headers().get("x-forwarded-proto");

  const data = await getData(`${proto}://${host}`)
  return <Main data={data}/>
}

 

