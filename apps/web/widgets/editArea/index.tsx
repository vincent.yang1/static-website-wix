'use client';

import React from 'react';
import { Button, Checkbox, Form, Input } from 'antd';
const { TextArea } = Input;


const EditArea = ({state, dispatch}) => {
  const [form] = Form.useForm();
  const idRef = React.useRef('')
  React.useEffect(()=>{
    document.querySelectorAll('[is-edit="1"]').forEach((item:any) => {
      item.onmouseover = ()=>{
        idRef.current = item.id
        dispatch({
          type:"change_dom_id",
          id: item.id
        })
      }
    })

  },[])


  React.useEffect(()=>{
    const data = state.tpl[idRef.current]
    if(data?.layout?.width){
      form.setFieldsValue({...data, width: data.layout.width, height: data.layout.height})
    } else {
      form.setFieldsValue({...data, width:'', height:""})
    }
  },[state])

  const onFinish = (values: any) => {
    console.log('Success:', values);
    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ "data": JSON.stringify(state.tpl)})
    };

    fetch("/api/tpl", options)
      .then(response => response.json())
      .then(data =>console.log(data));
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  const onChange = (attr, event) => {
    console.log(attr, event.target.value)
    const _tpl = {...state.tpl}
    if(Array.isArray(attr) && attr.length === 2){
      _tpl[state.activeId][attr[0]][attr[1]] = event.target.value
    } else {
      _tpl[state.activeId][attr] = event.target.value

    }
    dispatch({
      type:"change_tpl_data",
      tpl: _tpl
    })
  }

  return (
    <>
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="text"
          name="text"
        >
          <TextArea rows={4} onChange={e=>onChange('text', e)}/>
        </Form.Item>

        <Form.Item
          label="alt"
          name="alt"
        >
          <Input type="text" onChange={e=>onChange('alt', e)}/>
        </Form.Item>
        <Form.Item
          label="link"
          name="link"
        >
          <Input type="text" onChange={e=>onChange('link', e)}/>
        </Form.Item>
        
        <Form.Item
          label="source"
          name="source"
        >
          <Input type="text" onChange={e=>onChange('source', e)}/>
        </Form.Item>

        <Form.Item
          label="layout.width"
          name="width"
        >
          <Input type="text" onChange={e=>onChange(['layout', 'width'], e)}/>
        </Form.Item>
        
        <Form.Item
          label="layout.height"
          name="height"
        >
          <Input type="text" onChange={e=>onChange(['layout', 'height'], e)}/>
        </Form.Item>





        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
          <Button type="default" htmlType="button">
            Cancel
          </Button>

        </Form.Item>
      </Form>
    </>
  )
}



export default EditArea;