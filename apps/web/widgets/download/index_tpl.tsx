import React from 'react';

// import I18n from '@utils/i18n';
import Image from 'next/image'
import Link from 'next/link'


import styles from './index.module.css'

const DownloadArea = () => {

  return (
    <div className={styles.pageTop} key='web'>
      <div className={styles.contain}>
        <div className={styles.intro}>
          <Image
            src={require('../../images/download_intro.avif')}
            layout="responsive"
            width={607}
            height={532}
            alt=''
          />
        </div>

        <div className={styles.download}>
          <div>
            <div className={styles.line1}>
              行情交易 随时掌握
            </div>
            <div className={styles.line2}>通过我们的App，随时随地了解最新行情与项目，及时交易把握财富</div>
          </div>

          <div className={styles.downloadInfo}>
            <div className={styles.btnWrapper}>
              <Link legacyBehavior href="https://app.adjust.com/h7et2hs">
                <a target="_blank">
                  AppStore               
                </a>
              </Link>

              <Link legacyBehavior href="https://app.adjust.com/n2d212k">
                <a target="_blank">
                  Google Play
                </a>
              </Link>
            </div>
            <div className={styles.scanImg}>
              <Image
                src={require('../../images/qrcode.avif')}
                layout="responsive"
                width={118}
                height={118}
                alt=''
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DownloadArea
