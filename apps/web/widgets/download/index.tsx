import React from 'react';

// import I18n from '@utils/i18n';
// import Image from 'next/image'
import Link from 'next/link'
import Image from 'next/legacy/image'


import styles from './index.module.css'



// auto generated: tpl data start
  // let tpl_data

  // tpl_data = {
  //   'img-1': {
  //     layout: {
  //       "width": 607,
  //       "height": 532
  //     },
  //     "source":require("../../images/download_intro.avif"),
  //     "alt":"jj"
  //   },
  //   "txt-1": {
  //     text:"行情交易 随时掌握"
  //   },
  //   "txt-2": {
  //     text:"通过我们的App，随时随地了解最新行情与项目，及时交易把握财富"
  //   },
  //   "link-1": {
  //     href:"https://app.adjust.com/h7et2hs",
  //     text:"AppStore"
  //   },
  //   "link-2": {
  //     href: "https://app.adjust.com/n2d212k",
  //     text:"Google Play"
  //   },
  //   "img-2": {
  //     source: require('../../images/qrcode.avif'),
  //     layout: {
  //       width: 118,
  //       height: 118
  //     },
  //     "alt":"jj"
  //   }
  // }
// auto generated: tpl data end


export const  DownloadAreaContainer = ({tpl}) =>{

  return (
    <DownloadAreaPresent tpl_data={tpl}/>
  )
}
const DownloadAreaPresent = (props) => {
  // auto generated:
  
  // if(typeof window !== 'undefined' &&  window?.isEdit){
  //   tpl_data = props.tpl_data
  // }
  // auto generated:
  const {tpl_data} = props
  return (
    <div className={styles.pageTop} key='web'>
      <div className={styles.contain}>
        <div className={styles.intro}>
          <Image
            id='img-1'
            is-edit='1'
            src={tpl_data['img-1'].source}
            layout="responsive"
            width={tpl_data['img-1'].layout.width}
            height={tpl_data['img-1'].layout.height}
            alt={tpl_data['img-1'].alt}
          />
        </div>

        <div className={styles.download}>
          <div>
            <div id='txt-1' is-edit='1' className={styles.line1}>
              {tpl_data['txt-1'].text}
            </div>
            <div id='txt-2' is-edit='1' className={styles.line2}>
              {tpl_data['txt-2'].text}
            </div>
          </div>

          <div className={styles.downloadInfo}>
            <div className={styles.btnWrapper}>
              <Link   legacyBehavior href={tpl_data['link-1'].link}>
                <a is-edit='1' id='link-1'  target="_blank">
                  {tpl_data['link-1'].text}
                </a>
              </Link>

              <Link   legacyBehavior href={tpl_data['link-2'].link}>
                <a is-edit='1' id='link-2' target="_blank">
                  {tpl_data['link-2'].text}
                </a>
              </Link>
            </div>
            <div className={styles.scanImg}>
              <Image
                id='img-2'
                is-edit='1'
                src={tpl_data['img-2'].source}
                layout="responsive"
                width={Number(tpl_data['img-2'].layout.width)}
                height={Number(tpl_data['img-2'].layout.height)}
                alt={tpl_data['img-2'].alt}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

