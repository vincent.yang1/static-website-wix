'use client';

import { Button, Header } from "ui";
import { useReducer, useState, useEffect, useRef } from 'react';

import {DownloadAreaContainer} from '../download'
import EditArea from '../editArea'
import '../../styles/global.css';


function reducer(state, action) {
  if (action.type === 'init_tpl') {
    return {
      ...state,
      tpl: action.tpl
    };
  }
  if (action.type === 'change_dom_id') {
    return {
      ...state,
      activeId: action.id
    };
  }
  if (action.type === 'change_tpl_data') {
    return {
      ...state,
      tpl: action.tpl
    };
  }
  throw Error('Unknown action.');
}
const initState = { activeId:'', tpl: null}
export function Main({data}): JSX.Element {
  console.log('data', data.data.document_data)
  const [state, dispatch] = useReducer(reducer, {tpl: data.data.document_data, activeId:""});

  return (
    <>
      <DownloadAreaContainer tpl={state.tpl}/>
      <EditArea dispatch={dispatch} state={state}/>
    </>
  );
}
