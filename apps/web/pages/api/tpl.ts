const fs = require('fs')
const path = require('path')
const dataFilePath = process.env['NODE_ENV'] === "production" ? '/tmp/tpl.json' : path.join(process.cwd(), "/widgets/download/tpl.json");




// File destination.txt will be created or overwritten by default.
if(process.env['NODE_ENV'] === "production"){
  if (!fs.existsSync("/tmp/tpl.json")) {
    fs.mkdirSync("/tmp/", { recursive: true });

    fs.copyFile(path.join(process.cwd(), "/widgets/download/tpl.json"), "/tmp/tpl.json", (err) => {
      if (err) throw err;
      console.log('source.txt was copied to destination.txt');
    });
  }


}



export default function handler(req, res) {
  const { data } = req.body

  if (req.method === 'POST') {
    // Process a POST request
    const _data = JSON.parse(fs.readFileSync(dataFilePath, 'utf8')); 
    const document_data = JSON.parse(data)
    _data.data.document_data = document_data

    fs.writeFileSync(dataFilePath, JSON.stringify(_data));
    res.status(200).json({success:true})
  } else {

    // Handle any other HTTP method
    const _tpl = JSON.parse(fs.readFileSync(dataFilePath, 'utf8')); 
    res.status(200).json(_tpl)
  }

}